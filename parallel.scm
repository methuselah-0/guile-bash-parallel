#!/run/current-system/profile/bin/guile \
 -e main -s
!#
;; THIS FILE CAN ONLY BE RUN THROUGH cd <thisfile-dir> && scm "$(pwd)/thisfile"
;; OR: scm "/full/path/to/thisfile"
;; or possibly if you add it to your GUILE_LOAD_PATH
;; running it with guile "$(pwd)/thisfile" will fail.
(add-to-load-path "~/.guix-profile/share/guile/site/2.2")
(define-module (parallel)
 #:use-module (srfi srfi-1)
 #:use-module (rnrs io ports)
 #:use-module (sjson)
 #:use-module (sjson utils)
 #:use-module (fash)
 #:use-module (gnu bash)
 #:use-module (ice-9 textual-ports)
 #:use-module (ice-9 rdelim)
 #:use-module (ice-9 match)
 #:export (make-commands
           make-commands-file
           para-run
           serial-run
           ))

(define* (make-commands command
                        #:key (args-map '()) (args '()))
  (write-json-to-string `(@ ("command" ,command)
                              ("args-map" ,args-map)
                              ("args" ,args))))
(define* (make-commands-file file command
                        #:key (args-map '()) (args '()))
  (let* ((commands `(@ ("command" ,command)
                      ("args-map" ,args-map)
                      ("args" ,args)))
         (cmdString (write-json-to-string commands))
         (useless (system* "rm" file))        
         (outport (open-file-output-port file))
         )
    (put-string outport cmdString)
    (close-port outport)))
;;(define mycommands (make-commands "nmap" #:args-map '("-Pn" "-p" "{}" "{}") #:args '("25" "tutanota.com" "80" "posteo.de" "80" "apa bepa")))

;; TODO: uncomment
;;(make-commands-file "testfile2.json" "nmap" #:args-map '("-Pn" "-p" "{}" "{}") #:args '("25" "tutanota.com" "80" "posteo.de" "80" "apa bepa"))
;;(define teststring "testfile2.json")
;;(set! #$teststring teststring)

;;(define output #$(source /home/CHANGEME/src/code_bash/mapfork.sh && mapfork-json $teststring))
;; Display how sjson has read the results
;;(define scmJson (read-json-from-string output))

;; TODO: uncomment
;; (display scmJson)
;; (display "\n========== Pretty Print ==========\n")
;; ;; Pretty-print it to see that it looks sane 
;; (json-pprint (read-json-from-string output))

(define* (para-run command)
  (let* (
         (file "/tmp/para-run.tmp")
         (mapfork-path (string-append (dirname (current-filename)) "/mapfork.sh"))
         ;;(file "testfile2.json")
         (cmdString (write-json-to-string command))
         (useless (system* "rm" file))
         (outport (open-file-output-port file)))
    (put-string outport command)
    (close-port outport)
    ;; (display "showing command")
    ;; (display command)
    ;; (display "showing cmdString")
    ;; (display cmdString)
    (set! #$teststring file)
    ;;    (cmdString)))
    (set! #$mapforkpath mapfork-path)
    (read-json-from-string #$(source $mapforkpath && mapfork-json $teststring) #:use-fash #t)))

(define* (serial-run command)
  (let* (
         (file "/tmp/para-run.tmp")
         (mapfork-path (string-append (dirname (current-filename)) "/mapfork.sh"))
         ;;(file "testfile2.json")
         (cmdString (write-json-to-string command))
         (useless (system* "rm" file))
         (outport (open-file-output-port file)))
    (put-string outport command)
    (close-port outport)
    ;; (display "showing command")
    ;; (display command)
    ;; (display "showing cmdString")
    ;; (display cmdString)
    (set! #$teststring file)
    ;;    (cmdString)))
    (set! #$mapforkpath mapfork-path)
    (read-json-from-string #$(source $mapforkpath && mapfork-json $teststring --serial) #:use-fash #t)))

;; (display "Success!")
;; (display "\n")

;; (define mycommands (make-commands "nmap" #:args-map '("-Pn" "-p" "{}" "{}") #:args '("25" "tutanota.com" "80" "posteo.de" "80" "apa bepa")))
;; (define parResult (para-run mycommands))
;; (display "ParResult coming!\n")
;; (display parResult)
;; (display "ParResult end!\n")
