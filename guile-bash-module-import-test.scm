#!/run/current-system/profile/bin/guile \
 -e main -s
!#
;; THIS FILE CAN ONLY BE RUN THROUGH cd <thisfile-dir> && scm "$(pwd)/thisfile"
;; OR: scm "/full/path/to/thisfile"
;; or possibly if you add it to your GUILE_LOAD_PATH
;; running it with guile "$(pwd)/thisfile" will fail.

;; When guile and this module is installed properly you don't need either of below 2 path fixes.
(add-to-load-path "~/.guix-profile/share/guile/site/2.2")
(add-to-load-path (dirname (current-filename)))

(use-modules (parallel) (sjson) (sjson utils) (fash) (dsv) (ice-9 match)(ice-9 regex))
;;(define mycommands (make-commands "nmap" #:args-map '("-Pn" "-p" "{}" "{}") #:args '("25" "tutanota.com" "80" "posteo.de" "80" "apa bepa")))
(define list '("25" "tutanota.com" "25" "posteo.de" "443" "apa bepa"))
(define mycommands (make-commands "nmap" #:args-map '("-Pn" "-p" "{}" "{}") #:args list))
(define parResult (para-run mycommands))
(json-pprint parResult)
(define parResult (serial-run mycommands))
(json-pprint parResult)
;; (display parResult)
;; (display (fash->alist parResult))
;; (display (alist->atlist (map fash->alist parResult)))
;; (display (map fash->alist parResult))
;; (map (lambda (a) (match a ((b c d e) (match d ((f . g) (values (g))))))) (fash->alist parResult))
(display (map (lambda (a) (match:substring (string-match "nmap" (fash-ref a "command")))) parResult))
;; (scm->dsv-string (map fash->alist parResult))
