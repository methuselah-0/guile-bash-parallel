#!/bin/bash
# reference: https://stackoverflow.com/questions/13806626/capture-both-stdout-and-stderr-in-bash
nullWrap(){
    local -i i; i="$1"
    local myCommand="$2"
    local -a myCommandArgs="$3"
    local -a cmdAndArgs=("$myCommand" "(${myCommandArgs[*]@Q})")
    local myfifo="$4"
    local waitpid="$5"
    local stderr
    local stdout
    local stdret
    [[ -n "$5" ]] && until [[ ! -e /proc/"$waitpid" ]]; do sleep .2 ; done
    . <(\
	{ stderr=$({ stdout=$(eval "$myCommand ${myCommandArgs[*]@Q}"); stdret=$?; } 2>&1 ;\
	           declare -p stdout >&2 ;\
		   declare -p stdret >&2) ;\
	  declare -p stderr;\
	} 2>&1)
    local -a Arr=("(${cmdAndArgs[*]@Q})" "$stdout" "$stderr" "$stdret")
    # For testing serial vs parallel:
    #printf '%s\n' "waitpid is $waitpid , cmdAndArgs: ${cmdAndArgs[@]}" >> logfile
    #[[ -z "$5" ]] && printf '%s' 0 >&208
    [[ -z "$5" ]] && printf '%s' 0 >&$myfd
    (
	flock -x 207
	printf "${i}:%s\u0000" "(${Arr[*]@Q})" > "$myfifo"
    ) 207>/tmp/mapfork_write_lock_$PPID
}
mapfork(){
    local command serial json size
    command="$1"
    local -a CommandArgs="$2"
    if [[ -r "$3" ]];then local -a Args=$(cat "$3"); else local -a Args="${3}"; fi
    # TODO: shift 3 ; then parse the remaining arguments as possible option arguments.
    shift 3
    while [[ -n "$1" ]]; do
	if [[ "$1" == --serial ]]; then
	    serial="$1"
	    shift
	    continue
	elif [[ "$1" == --json ]]; then
	    json="$1"
	    shift
	    continue
	elif [[ "$1" == --size ]]; then
	    local -i size="$2" || return 1
	    shift 2
	    continue
	else
	    break
	fi
    done
    
    local -a PipedArr
    local -i i

    # Open a fifo and keep it open until done
    local myfifo; myfifo=$(mktemp /tmp/temp.XXXXXXXX) || { printf '%s\n' "mapfork: Failed to create tempfile" && return 1 ; }
    [[ -e "$myfifo" ]] && rm "$myfifo" ; mkfifo "$myfifo"
    trap '[[ -e $myfifo ]] && rm $myfifo' EXIT SIGTERM RETURN
    sleep infinity > "$myfifo" &
    spid=$!

    local -a placeHolders=()
    for ((i=0;i<${#CommandArgs[@]};i++)); do
	[[ "${CommandArgs[$i]}" =~ ^\{\}$ ]] && placeHolders+=("$i") ;done

    local waitpid
    local -i myfd
    open_sem(){
	[[ -r /tmp/pipe-$$ ]] || mkfifo /tmp/pipe-$$
	#exec 208<>/tmp/pipe-$$
	exec {myfd}<>/tmp/pipe-$$
	#rm /tmp/pipe-$$
	local i=$1
	for((;i>0;i--)); do
            #printf '%s' 0 >&208
	    printf '%s' 0 >&$myfd
	done
    }
    # run_with_lock(){
    # 	local x
    # 	# this read waits until there is something to read
    # 	read -u 3 -n 3 x #&& ((0==x)) || exit $x
    # 	(
    # 	    ( "$@"; )
    # 	    # push the return code of the command to the semaphore
    # 	    printf '%.3d' $? >&3
    # 	)&
    # }

    local -i cmdnum=0
    local ncpu x; ncpu=$(nproc)
    
    #exec 208<>&-
    #exec {myfd}<>/tmp/out.txt
    open_sem "${size:-$ncpu}"
    for ((i=0;i<${#Args[@]};i+=0)); do
	# if we have placeholders in CommandArgs we need to take args
	# from Args to replace.
	if [[ ${#placeHolders[@]} -gt 0 ]]; then
	    for ii in "${placeHolders[@]}"; do
		CommandArgs["$ii"]="${Args[$i]}"
		i+=1; done; fi
	if [[ "$serial" = --serial ]] ; then
	    nullWrap "$i" "$command" "(${CommandArgs[*]@Q})" "$myfifo" "$waitpid" &
	    waitpid=$!
	    cmdnum+=1
	else
	    #nullWrap "$i" "$command" "(${CommandArgs[*]@Q})" "$myfifo" &
	    #read -u 208 -n 1 x
	    read -u $myfd -n 1 x
	    nullWrap "$i" "$command" "(${CommandArgs[*]@Q})" "$myfifo" &
	    cmdnum+=1
	fi
    done
    #for ((i=0;i<${#Args[@]};i+=$(("${#placeHolders[@]}")))) ; do
    for ((i=0;i<$cmdnum;i+=1)) ; do
	local res
	# TODO: fix - may not need to create subshell
	res=$(read -rd '' temp <"$myfifo" && printf '%b' "$temp")
	local -i resI
	resI="${res%%:*}"
	PipedArr[$resI]="${res#*:}"
    done
    #exec $myfd<&-
    #exec $myfd>&-
    kill -9 "$spid" &>/dev/null
    [[ -e "$myfifo" ]] && rm "$myfifo"
    #exec 208>&-
    #exec 208<&-
    
    # reference: https://stackoverflow.com/questions/41966140/how-can-i-make-an-array-of-lists-or-similar-in-bash

    if [[ -n "$json" ]] && [[ "$json" = --json ]]; then
	if mapfork_results_to_json "(${PipedArr[*]@Q})"; then eval "exec $myfd>&-" && rm /tmp/pipe-$$ && return 0; else exec $myfd>&- && exec $myfd<&- && return 1; fi
	# local -a Results=$(printf '%s' "(${PipedArr[*]@Q})")
	# for ((i=0;i<${#Results[@]};i++)); do
	#     local -a result="${Results[$i]}"
	#     local -a cmd_args="${result[0]}"
	#     cmd="${cmd_args[0]}"
	#     local -a args="${cmd_args[1]}"
	#     jq -c -n --unbuffered \
	#        --arg stdout "${result[1]}" \
	#        --arg stderr "${result[2]}" \
	#        --arg ret "${result[3]}" \
	#        --arg args "$(printf '%s' "${cmd[0]@Q} " "${args[*]@Q}")" \
	#        '. | { command: $args , stdout: $stdout , stderr: $stderr , "return-code": $ret }'; done | jq -s .
	# return 0
    else
	printf '%s' "(${PipedArr[*]@Q})" && exec $myfd>&- && exec $myfd<&-;fi
}
mapfork-json(){
    [[ -r "$1" ]] || return 1
    jsonStripQuotes(){ local t0; while read -r t0; do t0="${t0%\"}"; t0="${t0#\"}"; printf '%s\n' "$t0"; done < <(jq '.');}
    mapfile -t command < <(jq -c '.command' <"$1" | jsonStripQuotes)
    mapfile -t Args < <(jq -c '.args[]' <"$1" | jsonStripQuotes)
    mapfile -t ArgsMap < <(jq -c '."args-map"[]' <"$1" | jsonStripQuotes)
    if [[ "$2" = "--serial" ]]; then
	mapfork "$command" "(${ArgsMap[*]@Q})" "(${Args[*]@Q})" --serial --json
    else
	mapfork "$command" "(${ArgsMap[*]@Q})" "(${Args[*]@Q})" --json
    fi
}
mapfork_results_to_json(){
    trap 'for a in mapfork_stdout.$$ mapfork_stderr.$$ mapfork_ret.$$ mapfork_args.$$ mapfork_write_lock_$PPID ; do [[ -e /tmp/$a ]] && rm /tmp/$a; done' EXIT SIGTERM RETURN
    local -a Results="$1" || return 1
    for ((i=0;i<${#Results[@]};i++)); do
	local -a result="${Results[$i]}"
	local -a cmd_args="${result[0]}"
	cmd="${cmd_args[0]}"
	local -a args="${cmd_args[1]}"
	printf '%s' "${result[1]}" > /tmp/mapfork_stdout.$$
	printf '%s' "${result[2]}" > /tmp/mapfork_stderr.$$
	printf '%s' "${result[3]}" > /tmp/mapfork_ret.$$
	printf '%s' "${cmd[0]@Q} " "${args[*]@Q}" > /tmp/mapfork_args.$$
	jq -c -n --unbuffered \
	   --rawfile stdout /tmp/mapfork_stdout.$$ \
	   --rawfile stderr /tmp/mapfork_stderr.$$ \
	   --rawfile ret /tmp/mapfork_ret.$$ \
	   --rawfile args /tmp/mapfork_args.$$ \
	   '. | { command: $args , stdout: $stdout , stderr: $stderr , "return-code": $ret }'; done | jq -s .
    # jq -c -n --unbuffered \
	#    --arg stdout "${result[1]}" \
	#    --arg stderr "${result[2]}" \
	#    --arg ret "${result[3]}" \
	#    --arg args "$(printf '%s' "${cmd[0]@Q} " "${args[*]@Q}")" \
	#    '. | { command: $args , stdout: $stdout , stderr: $stderr , "return-code": $ret }'; done | jq -s .
}
